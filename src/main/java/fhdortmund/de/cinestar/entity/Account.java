package fhdortmund.de.cinestar.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column(name="kundeid")
	private Kunde kunde = null;
	@Column(name="emailAdresse")
	private String emailAdresse = null;
	@Column(name="passwort")
	private String passwort = null;

	public Account(String emailAdresse, String passwort) {
		this.emailAdresse = emailAdresse;
		this.passwort = passwort;
	}

	public Account(String emailAdresse, String passwort, Kunde kunde) {
		this.emailAdresse = emailAdresse;
		this.passwort = passwort;
		this.kunde = kunde;
	}

	public Kunde getKunde() {
		return kunde;
	}

	public String getEmailAdresse() {
		return emailAdresse;
	}

	public void setEmailAdresse(String emailAdresse) {
		this.emailAdresse = emailAdresse;
	}

	public String getPasswort() {
		return passwort;
	}

	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}


	public boolean match(Account k) {
		return true;
	}

	public boolean equals(Object k) {
		return true;
	}


	public int hashCode() {
		return 0;
	}

	public String toString() {
		return "Emailadresse = " + emailAdresse + ", Passwort = " + passwort;
	}

}
