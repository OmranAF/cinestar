package fhdortmund.de.cinestar.filmverwaltung;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import fhdortmund.de.cinestar.datahaltung.IProgrammDAO;


public class Programmverwaltung {
	

		private SortedSet<Programm> programmListe;
		private IProgrammDAO dao;

		public Programmverwaltung(IProgrammDAO dao) {
			programmListe = new TreeSet<Programm>();
			this.dao = dao;
		}

		
		public void laden() throws IOException, ProgrammBereitsVorhandenException {
			programmListe.clear();
			try {
				List<Programm> liste = dao.laden();
				for (Programm a : liste)
					this.addProgramm(a);

			} catch (ProgrammBereitsVorhandenException ex) {
				throw new ProgrammBereitsVorhandenException(
						"Fehler beim Laden der Artikeldaten. Es gibt zwei Artikel mit derselben ID!");
			}
		}

		public void speichern() throws IOException {
			List<Programm> liste = new ArrayList<>();
			for (Programm a : programmListe)
				liste.add(a);
			dao.speichern(liste);
		}

		public boolean istLeer() {
			return programmListe.isEmpty();
		}

		public List<Programm> getProgrammliste(Kategorie kategorie) {
			ArrayList<Programm> programmLi = new ArrayList<Programm>();
			for (Programm programm : programmListe) {
				if (programm.getKategorie().equals(kategorie))
					programmLi.add(programm);
			}
			return programmLi;
		}
		
		public List<Programm> getProgrammliste() {
			ArrayList<Programm> programmLi = new ArrayList<Programm>();
			for (Programm programm : programmListe) {
				programmLi.add(programm);
			}
			return programmLi;
		}

				public void addProgramm(Programm programm) throws ProgrammBereitsVorhandenException {
			if (!programmListe.add(programm)) {
				String str = "Programm kann nicht hinzugef�gt werden,\n"
						+ "da bereits ein Programm mit derselben ID existiert:\n" + programm.toString();
				throw new ProgrammBereitsVorhandenException(str);
			}
			updateKatLists(programm);
		}

		private void updateKatLists(Programm programm) {
		}
	}
