package fhdortmund.de.cinestar.filmverwaltung;


public class Programm implements Comparable<Programm> {
	
	private static int letzteArtikelnummer = 0;
	private int programmnummer;
	private Film film;
	private Saal saal;
	private String showTime;
	private static int verfuegbarPlatz;
	private int anzReservierung = 0;
	private Kategorie kategorie;
	private int prise;
		
	
	public int getPrise() {
		return prise;
	}


	public void setPrise(int prise) {
		this.prise = prise;
	}


	public Programm(Film film , Saal saal, String showTime,int prise) {
		
	setVerfuegbarPlatz(saal.getAnzPlatz());
	setShowTime(showTime);
	this.film = film;
	this.kategorie=film.getKategorie();
	this.prise=prise;
	
	}
	
	
	public String getShowTime() {
		return showTime;
	}
	public void setShowTime(String showTime) {
		this.showTime = showTime;
	}
	public int getVerfuegbarPlatz() {
		return verfuegbarPlatz;
	}
	public void setVerfuegbarPlatz(int verfuegbarPlatz) {
		this.verfuegbarPlatz = verfuegbarPlatz;
	}
	public int getAnzReservierung() {
		return anzReservierung;
	}
	public void setAnzReservierung(int anzReservierung) {
		this.anzReservierung = anzReservierung;
	}
	public Film getFilm() {
		return film;
	}
	public Saal getSaal() {
		return saal;
	}

	public Kategorie getKategorie() {
		
		return this.kategorie;
	}


	public int getProgrammnummer() {
	return this.programmnummer;
	}


	public int compareTo(Programm a) {
		if (a == null)
			throw new NullPointerException("Zu vergleichender Artikel ist null");
		int erg = this.kategorie.compareTo(a.getKategorie());
		if (erg == 0) // Sortierung nach Artikelnummer vornehmen
			erg = this.getFilm().getFilmId() - a.getFilm().getFilmId();
		return erg;
	}
	

}
