package fhdortmund.de.cinestar.fachlogik;

import java.util.SortedMap;
import java.util.TreeMap;

import fhdortmund.de.cinestar.filmverwaltung.Programm;


public class Einkaufswagen {

private SortedMap<Programm, Integer> programmliste = new TreeMap<Programm, Integer>();
	

	public void einfuegen(Programm programm) {
		if (programmliste.containsKey(programm)) {
			Integer value = programmliste.get(programm);
			programmliste.put(programm, value + 1);
		} else
			programmliste.put(programm, 1);
	}
	
	
	public boolean loeschen(Programm programm) {
		boolean OK = true;
		if (programmliste.containsKey(programm)) {
			Integer value = programmliste.get(programm);
			if (value > 1)
				programmliste.put(programm, value - 1);
			else
				programmliste.remove(programm);
		} else
			OK = false;
		return OK;
	}

	public void allesLoeschen() {
		programmliste = new TreeMap<Programm, Integer>();
	}	
	
	public SortedMap<Programm, Integer> getInhalt() {
		return (SortedMap<Programm, Integer>)((TreeMap<Programm, Integer>)programmliste).clone();
	}
	
	public boolean istLeer() {
		return programmliste.isEmpty();
	}

}

