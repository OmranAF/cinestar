package fhdortmund.de.cinestar.datahaltung;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fhdortmund.de.cinestar.entity.Account;
import fhdortmund.de.cinestar.entity.Kunde;
import fhdortmund.de.cinestar.kundeverwaltung.Kundeverwaltung;



public class AccountDAO implements IAccountDAO {

	private void initDaten(List<Account> accountListe) {
		Kunde kunde;
		Account account;
		kunde = new Kunde("Cine Star Admin");
		account = new Account("test", "test", kunde);
		accountListe.add(account);

	}
	public List<Account> laden() throws IOException {
		try {
			List<Account> liste = new ArrayList<>();
			initDaten(liste); // Da noch keine IO bekannt
			return liste;
		} catch (NullPointerException e) {
			throw new IOException("Fehler beim Laden der Kundendaten: " + e.getMessage());
		}
	}


	public void speichern(List<Account> accountListe) throws IOException {
		
	}
}
