package fhdortmund.de.cinestar.datahaltung;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fhdortmund.de.cinestar.filmverwaltung.Film;
import fhdortmund.de.cinestar.filmverwaltung.Kategorie;
import fhdortmund.de.cinestar.filmverwaltung.Programm;
import fhdortmund.de.cinestar.filmverwaltung.Saal;
import fhdortmund.de.cinestar.kundeverwaltung.UngueltigerNameException;



public class ProgrammDAO implements IProgrammDAO {
	
	private void initDaten(List<Programm> programmListe)
			throws UngueltigerNameException {//muss die Exception ändern 
		//muss die Programm konstruktor einverstanden
		
		
		programmListe.add(new Programm(new Film(Kategorie.Action,"Action Film ","Advenger"), new Saal("A.00",150),"Thu 10.00",7 ));
		programmListe.add(new Programm(new Film(Kategorie.Drama,"Action Film ","Romoio"), new Saal("A.10",150),"Fri 13.00",5 ));
		programmListe.add(new Programm(new Film(Kategorie.Drama,"Action Film ","Limar"), new Saal("A.02",150),"Wed 17.00",9 ));
		programmListe.add(new Programm(new Film(Kategorie.Horo,"Action Film ","Taitanik"), new Saal("A.01",150),"Thu 21",8 ));
		programmListe.add(new Programm(new Film(Kategorie.Comedy,"Action Film ","Lumbi"), new Saal("A.03",150),"Tue 20.00",7 ));
		programmListe.add(new Programm(new Film(Kategorie.Action,"Action Film ","Saher"), new Saal("A.01",150),"Wed 18.00",7 ));
	}
	


	public List<Programm> laden() throws IOException {
		List<Programm> liste = new ArrayList<Programm>();
		try {
			initDaten(liste);
			return liste;
		} catch (UngueltigerNameException e) {
			throw new IOException("Fehler beim Laden der Programmdaten:"
					+ e.getMessage());
		}
	}
	public void speichern(List<Programm> programmListe) throws IOException {
		
	}
}
