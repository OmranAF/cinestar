package fhdortmund.de.cinestar.kundeverwaltung;

public class UngueltigerNameException extends Exception {

	public UngueltigerNameException() {
		super();
	}

	public UngueltigerNameException(String arg0) {
		super(arg0);
	}
	

}
