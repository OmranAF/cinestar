package fhdortmund.de.cinestar.ui;

import javax.swing.RowFilter;
import javax.swing.RowFilter.Entry;

import fhdortmund.de.cinestar.filmverwaltung.Kategorie;





public class KategorienFilter extends RowFilter<MyTableModel, Integer> {

	private Kategorie kategorie;

	public KategorienFilter(Kategorie kategorie) {
		this.kategorie = kategorie;
	}

	public boolean include(Entry<? extends MyTableModel, ? extends Integer> entry) {
		Kategorie kategorie = (Kategorie) entry.getValue(0);
		if (this.kategorie == Kategorie.Alle)
			return true;
		else
			return kategorie == this.kategorie;
	}
}
