package fhdortmund.de.cinestar.ui;

import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class MyTableModel extends DefaultTableModel {
	public MyTableModel(Object[] columnNames, int rowCount) {
		super(columnNames, rowCount);
	}
	
	MyTableModel(Object[][] rows, String[] columns) {
		super(rows, columns);
	}
	
	public Class<?> getColumnClass(int column) {
		if (getRowCount() > 0 && column >= 0 && column < getColumnCount())
			return getValueAt(0, column).getClass();
		else
			return Object.class;
	}
}
