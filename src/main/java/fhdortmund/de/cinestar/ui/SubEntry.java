package fhdortmund.de.cinestar.ui;

import java.util.Map.Entry;

public class SubEntry<K, V> implements Entry<K, V> {
	private K key;
	private V value;

	public SubEntry(K key, V value) {
		this.key = key;
		this.value = value;
	}

	public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}

	public V setValue(V value) {
		V oldvalue = this.value;
		this.value = value;
		return oldvalue;
	}

	public String toString() {
		return key.toString() + "\t || Anzahl: " + value.toString();
	}

}
